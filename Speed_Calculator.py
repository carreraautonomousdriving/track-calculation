#!/usr/bin/python
#-*- coding: utf-8 -*-
#Obiges ist elementar wichtig!


#TESTWEISE:
streckenteile = [15,16,11,15,16,10,13]
spannungen = []
pulse = []
frequenzPWM = 20000


##Temporäre Liste erstellen, mit der gearbeitet werden kann, ohne dass die Originalliste verändert wird
#global tempList
#tempList = streckenteile[:]


#Ich benoetige:
#- Reibungskoeffizient. Vllt kann man auch experimentell die maximale Geschwindigkeit in den Kurven erhalten.
#- (negative) Beschleunigung der Autos [Gyrosensor?]
#- Zeit, die es fuer die einzelnen Teile braucht
#- Optimal waere ein Feedback von Lichtschranken in der Bahn, dass man weiß wo sich das Auto befindet [Findet die Berechnung der Spannung in Echtzeit statt? Ansonsten bringt das Feedback auch nichts...]
#- Name der Input Liste. Aktuell heißt sie "streckenteile". Und wie sind die unterscheidlichen Streckenteile benannt?
#- Wie greife ich mit Python auf eine Datenbank zu?
#- In welchem Abstand sind die Impulse? Damit ich die Anzahl der Impulse berechnen kann.
#---> 10ms
#- Laenge einer kurzen und einer langen Gerade?
#---> kurz: 115mm und lang: 345mm
#- "geschwindigkeitKurve" (bei 6V), "beschleunigung", Bremsbeschleunigung "bremsen" fehlen alle noch


#TODO: - Name einer Liste erhalten
#- Listennamen in DB suchen
#- Trackelemente aus DB in Liste 'streckenteile[]' auslesen


#Streckenteile aus der Liste "streckenteile" aus Zahl in Name konvertieren
def auslesen(argument):
    switcher = {
        10: "Short_Curve_R",
        11: "Short_Curve_L",
        12: "Long_Curve_R",
        13: "Long_Curve_L",
        14: "Short_Straight",
        15: "Long_Straight",
        16: "Start"
    }
    return switcher.get(argument, "Invalid")
    #print switcher.get(argument, "Kein reales Streckenteil")



#Gibt an wie viele aufeinanderfolgende Teile kommen
#RUNS. DON'T TOUCH!!!
def anzahlGeraden():
    anzahl = 0
    while len(streckenteile) > 0:
        #"Short_Straight":
        if streckenteile[0] == 14:
            anzahl = anzahl + 1
            streckenteile.remove(streckenteile[0])
        #"Long_Straight" und "Start"
        elif streckenteile[0] == 15 or streckenteile[0] == 16:
            anzahl = anzahl + 3
            streckenteile.remove(streckenteile[0])
        else:
            return anzahl
    return anzahl



#Berechnet die Zeit auf der Geraden
def zeitAufGeraden(anzahlGleicherTeile):
    zeit = [0,0]
    #Beschleunigungszeit:
    #"geschwindigkeitKurve" (bei 6V), "beschleunigung", Bremsbeschleunigung "bremsen" fehlen alle noch
    zeit[0] = 12 #geschwindigkeitKurve * ((-1 + math.sqrt(1 - (2 * anzahlGleicherTeile * 11,5 * beschleunigung)/((1 + beschleunigung/bremsen) * geschwindigkeitKurve * geschwindigkeitKurve))) / beschleunigung)
    #Abbremszeit:
    zeit[1] = 3333 #geschwindigkeitKurve * (beschleunigung/bremsen) * ((-1 + math.sqrt(1 - (2 * anzahlGleicherTeile * 11,5 * beschleunigung)/((1 + beschleunigung/bremsen) * geschwindigkeitKurve * geschwindigkeitKurve))) / beschleunigung)
    return zeit



def anfügen(zeitInS, spannungInV):
    pulse.append(zeitInS)
    spannungen.append(spannungInV)



#TODO: Funktion, die schaut ob nach einer Kurve eine Gerade kommt und ändert dementsprechend pulse[] an den jeweiligen Elementen.
#Am Ende der Gerade soll das Auto bereits beschleunigen (Kurvenzeit kürzer) und dementsprechend länger bremsen vor der
#nächsten Kurve (Wahrscheinlich am sinnvollsten, wenn die Kurvenzeit verkürzt wird und die Gerade als länger angesehen wird
#und dementsprechend auch Beschleunigungs- und Bremszeit mit zeitAufGeraden() berechnet werden.)



#Umrechnung in Listen mit Zeit und Spannung
def main_calculation():

    while len(streckenteile) > 0:

        #TODO: Zuerst kommt die Zielgerade. Weil das Auto erst losfaehrt kann man es vermutlich wie eine normal lange Gerade behandeln weil es erst beschleunigen muss.
        #auslesen(streckenteile)

        if streckenteile[0] == 10: #"Short_Curve_R":
            #TODO: Zeit messen, wie lange das Auto bei 6V in der kurzen Rechtskurve braucht
            anfügen(42,6)
            streckenteile.remove(streckenteile[0])
        elif streckenteile[0] == 11: #"Short_Curve_L":
            #TODO: Zeit messen, wie lange das Auto bei 6V in der kurzen Linkskurve braucht
            anfügen(42,6)
            streckenteile.remove(streckenteile[0])
        elif streckenteile[0] == 12: #"Long_Curve_R":
            #TODO: Zeit messen, wie lange das Auto bei 6V in der langen Rechtskurve braucht
            anfügen(42,6)
            streckenteile.remove(streckenteile[0])
        elif streckenteile[0] == 13: #"Long_Curve_L":
            #TODO: Zeit messen, wie lange das Auto bei 6V in der langen Linkskurve braucht
            anfügen(42,6)
            streckenteile.remove(streckenteile[0])
        elif streckenteile[0] == 14 or streckenteile[0] == 15: #"Short_Straight" OR "Long_Straight"
            zeit = zeitAufGeraden(anzahlGeraden())
            #Bei 14,8V:
            anzahlPulsSignale = zeit[0] * frequenzPWM
            anfügen(anzahlPulsSignale , 14.8)
            #Bei 0V:
            anzahlPulsSignale = zeit[1] * frequenzPWM
            anfügen(anzahlPulsSignale , 0)
        elif streckenteile[0] == 16:    #"Start":
            #TODO: Startelement soll beim ersetn Durchlauf für bestimmte Zeit mit 6V (vllt ein kleines Bisschen mehr) betrieben
            #werden, damit er gut beschleunigt, aber nicht zu schnell in die erste Kurve fährt.
            #Nach das Auto zum ersten Mal die Lichtschranke passiert soll die Startgerade wie eine normale Gerade behandelt werden.
            pass
        else:
            #Falls eine falsche Zahl reingerutscht ist.
            streckenteile.remove(streckenteile[0])

#TODO:
#Kurven:
#- Konstante Spannung berechnen (Es werden 6V angenommen)
#- Innen oder außen? (Aktuell noch kein Unterschied, aber experimentell muss die maximale Geschwindigkeit pro Spur ermittelt werden)
#- Zeit fuer Kurve messen
#- Beschleunigung aus der Kurve heraus???
#
#Geraden:
#- Maximale geschwindigkeit messen
#- Volle Beschleunigung messen
#- Volle Abbremsung messen
#- Zeiten fuer Beschleunigung und Abbremsung berechnen (siehe Formel auf Blatt)
#
#- Beim Start wird eine Liste an Befehlen ausgegeben
#- Beim Durchfahren der Lichtschranke soll eine andere Liste an Befehlen ausgegeben werden, die ab dann für jede Runde verwendet wird.
#- Nach einer gewissen Anzahl an durchläufen wird die Liste nicht mehr ausgegeben




main_calculation()

#if len(pulse) != len(spannungen):
#print("Fatal Error! Spannungs- und Puls-Listen sind nicht gleich lang.")
#else:
#print("Liste pulse ganze am Ende: ", pulse)
#print("Liste spannungen ganze am Ende: ", spannungen)


#Ausgabe sind 2 Listen. Eine mit den Spannungen und eine mit der Anzahl wie lange mit besagter Spannung gepulst werden soll.