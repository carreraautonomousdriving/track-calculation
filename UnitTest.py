import unittest
from Speed_Calculator import auslesen, anzahlGeraden

class UnitTest_Speed_Calculator(unittest.TestCase):
    def test_auslesen(self):
        self.assertEqual(auslesen(12), "Long_Curve_R")

    def test_anzahlGeraden(self):

        self.assertEqual(anzahlGeraden(), 6)


if __name__ == '__main__':
    unittest.main()